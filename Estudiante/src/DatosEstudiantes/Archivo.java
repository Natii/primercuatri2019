package DatosEstudiantes;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author Natalia
 */
public class Archivo {

    File rutaUsuarios = new File("C:\\Users\\Natalia\\Documents\\Universidad\\2019\\ICuatri\\Progra2\\Estudiante\\Usuarios.txt");
    File rutaNotas = new File("C:\\Users\\Natalia\\Documents\\Universidad\\2019\\ICuatri\\Progra2\\Estudiante\\Notas.txt");

    public void IngresarArchivoSuperUsuario(String nombre, String contra) {
        FileWriter fw;
        BufferedWriter bw;
        try {
            if (rutaUsuarios.exists()) {
                fw = new FileWriter(rutaUsuarios, true);
                bw = new BufferedWriter(fw);
                bw.write(nombre + ", " + contra + "\n");
            } else {
                fw = new FileWriter(rutaUsuarios, true);
                bw = new BufferedWriter(fw);
                bw.newLine();
                bw.write(nombre + ", " + contra + "\n");
            }
            bw.close();
            fw.close();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Ingresó con éxito");
        }
    }

    public void IngresarArchivoNota(String nombre, String apellido1, String nota1, String nota2, String nota3) {
        
        FileWriter fw;
        BufferedWriter bw;
        try {
            if (rutaNotas.exists()) {
                fw = new FileWriter(rutaNotas, true);
                bw = new BufferedWriter(fw);
                bw.write(nombre + ", " + apellido1 + ", " + nota1 + ", " + nota2 + ", " + nota3 + "\n");
            } else {
                fw = new FileWriter(rutaNotas, true);
                bw = new BufferedWriter(fw);
                bw.newLine();
                bw.write(nombre + ", " + apellido1 + ", " + nota1 + ", " + nota2 + ", " + nota3 + "\n");
            }
            bw.close();
            fw.close();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Ingresó las notas con éxito");
        }
    }

    public void EscribirArchivo(String nombreArchivo, String tipoTexto) {
        try {
            FileWriter fw = new FileWriter(rutaUsuarios, true);
            FileWriter f = new FileWriter(rutaNotas, true);
            BufferedWriter bw = new BufferedWriter(fw);
            BufferedWriter b = new BufferedWriter(fw);
            rutaUsuarios = new File(nombreArchivo);
            rutaNotas = new File(nombreArchivo);
            bw.write(tipoTexto);
            b.write(tipoTexto);
            bw.close();
            b.close();
            fw.close();
            f.close();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Error" + e);
        }
    }

    public void LeerArchivo(String nombreArchivo) throws FileNotFoundException, IOException {
        FileReader r = new FileReader(nombreArchivo);
        BufferedReader buffer = new BufferedReader(r);
        String temp = " ";
        while (temp != null) {
            temp = buffer.readLine();
            if (temp == null) {
                break;
            }
            System.out.println(temp);
        }
    }
}
