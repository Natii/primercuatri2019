package Negocios;

import DatosEstudiantes.Archivo;

/**
 *
 * @author Natalia
 */
public class LogicaNotas {

    String nombre = "";
    String apellido1 = "";
    double nota1 = 0;
    String notaUno = String.valueOf(nota1);
    double nota2 = 0;
    String notaDos = String.valueOf(nota2);
    double nota3 = 0;
    String notaTres = String.valueOf(nota3);
    double promedio = 0;
    String prome = String.valueOf(promedio);

    public void guardarNotas() {

        Archivo archi = new Archivo();
        archi.IngresarArchivoNota(nombre, apellido1, notaUno, notaDos, notaTres);
    }

    public double calcularPromedio() {
        promedio = (nota1 + nota2 + nota3) / 3;
        
        return promedio;
    }

}
