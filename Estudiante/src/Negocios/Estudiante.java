package Negocios;

/**
 *
 * @author Natalia
 */
public class Estudiante {
    
    String nombre;
    String apellido;
    String nota1;
    String nota2;
    String nota3;
    String promedio;
    
    public Estudiante() {
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getNota1() {
        return nota1;
    }

    public String getNota2() {
        return nota2;
    }

    public String getNota3() {
        return nota3;
    }

    public String getPromedio() {
        return promedio;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setNota1(String nota1) {
        this.nota1 = nota1;
    }

    public void setNota2(String nota2) {
        this.nota2 = nota2;
    }

    public void setNota3(String nota3) {
        this.nota3 = nota3;
    }

    public void setPromedio(String promedio) {
        this.promedio = promedio;
    }
        
}
