package Negocios;

import java.util.ArrayList;

/**
 *
 * @author Natalia
 */
public class Proceso {

    private ArrayList<Object> a = new ArrayList<Object>();

    public Proceso() {
    }

    public Proceso(ArrayList<Object> a) {
        this.a = a;
    }

    public void agregarRegistro(Estudiante e) {
        this.a.add(e);
    }

    public void modificarRegistro(int i, Estudiante e) {
        this.a.set(i, e);
    }

    public void eliminarRegistro(int i) {
        this.a.remove(i);
    }

    public Estudiante obtenerRegistro(int i) {
        return (Estudiante) a.get(i);
    }

    public int cantidadRegistro() {
        return this.a.size();
    }

    public int buscaCodigo(String codigo) {
        for (int i = 0; i < cantidadRegistro(); i++) {
            if (codigo.equals(obtenerRegistro(i).getNombre())) {
                return i;
            }
        }
        return -1;
    }

}
