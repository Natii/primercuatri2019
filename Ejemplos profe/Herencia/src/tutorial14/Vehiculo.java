package tutorial14;

public class Vehiculo {

    public String marca, color, matricula;
    public double precio;

    public Vehiculo(String marca, String color, double precio, String matricula) {
        this.color = color;
        this.marca = marca;
        this.matricula = matricula;
        this.precio = precio;
    }

    public String getAtributos() {
        return "Marca: " + marca
                + "\nColor: " + color
                + "\nMatricula:" + matricula
                + "\nPrecio: " + precio;
    }

    //polimorfismo
    public void tipoVehiculo() {
        System.out.println("Qué tipo de vehículo es?");
    }
}
