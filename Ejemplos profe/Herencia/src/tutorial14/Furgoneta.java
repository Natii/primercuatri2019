package tutorial14;

public class Furgoneta extends Vehiculo {

    private int capacidadPersonas;
    boolean disponible;

    public Furgoneta(int capacidadPersonas, boolean disponible, String marca, String color, double precio, String matricula) {
        super(marca, color, precio, matricula);
        this.capacidadPersonas = capacidadPersonas;
        this.disponible = disponible;

    }

    public String getAtributos() {
        return "Marca: " + marca
                + "\nColor: " + color
                + "\nMatricula:" + matricula
                + "\nPrecio: " + precio
                + "\nCapacidad de presonas: " + capacidadPersonas
                + "\nDisponibilidad: " + disponible;
    }

    //polimorfismo
    public void tipoVehiculo() {
        System.out.println("el vehículo es una furgoneta");
    }
}
