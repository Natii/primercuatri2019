package tutorial14;

public class Tutorial14 {

    public static void main(String[] args) {

        Vehiculo auto1 = new Vehiculo("Toyota", "Rojo", 20000, "PQR-458");

        Deportivo auto2 = new Deportivo("Mazda", "Negro", 50000, "SJU-954", 250.0, 180);
        Furgoneta auto3 = new Furgoneta(30, false, "Audi", "blanco", 30000, "HFR-127");

        System.out.println(auto1.getAtributos());
        System.out.println("");
        System.out.println(auto2.getAtributos());
        tipo(auto2);
        System.out.println("");
        System.out.println(auto3.getAtributos());
        tipo(auto3);
    }
    
    //polimorfismo
    public static void tipo (Vehiculo vehiculo){
        vehiculo.tipoVehiculo();
    }
}
