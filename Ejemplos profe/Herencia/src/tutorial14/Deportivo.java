package tutorial14;

public class Deportivo extends Vehiculo {

    double velMaxima, potencia;
    
    public Deportivo(String marca, String color, double precio, String matricula, double velMaxima, double potencia) {
        super(marca, color, precio, matricula);
        this.potencia = potencia;
        this.velMaxima = velMaxima;
    }

    public String getAtributos() {

        return "Marca: " + marca
                + "\nColor: " + color
                + "\nMatricula:" + matricula
                + "\nPrecio: " + precio
                + "\nPotencia: " + potencia
                + "\nVelocidad Maxima: " + velMaxima;
    }

    //polimorfismo
    public void tipoVehiculo() {
        System.out.println("el vehículo es deportivo");
    }
}
