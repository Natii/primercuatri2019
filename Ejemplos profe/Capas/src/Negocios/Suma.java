package Negocios;

import Datos.Archivo;

public class Suma {

    public int calcularSuma() {
        int valor1, valor2;
        
        Archivo archivo = new Archivo();
        String resultadoArchivo = archivo.arch();
        
        String[] numeros = resultadoArchivo.split(",");
        
        valor1 = Integer.parseInt(numeros[0]);
        valor2 = Integer.parseInt(numeros[1]);
        int suma = valor1 + valor2;
        return suma;
    }
}
