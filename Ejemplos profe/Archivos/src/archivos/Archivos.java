package archivos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

public class Archivos {

    public static void main(String[] args) {
        Random k = new Random();
        Random t = new Random();
        int lista[] = new int[5];
        int lista2[] = new int[5];
        int lista3[] = new int[10];
        for (int i = 0; i < lista.length; i++) {
            lista[i] = k.nextInt(20);
            for (int j = 0; j < i; j++) {
                if (lista[i] == lista[j]) {
                    i--;
                }
            }
        }

        for (int y = 0; y < lista2.length; y++) {
            lista2[y] = t.nextInt(20);
            for (int a = 0; a < y; a++) {
                if (lista2[y] == lista2[a]) {
                    y--;
                }
            }
        }

        for (int o = 0; o < lista3.length; o++) {
            lista3[0] = lista[0];
            lista3[1] = lista[1];
            lista3[2] = lista[2];
            lista3[3] = lista[3];
            lista3[4] = lista[4];
            lista3[5] = lista2[0];
            lista3[6] = lista2[1];
            lista3[7] = lista2[2];
            lista3[8] = lista2[3];
            lista3[9] = lista2[4];
        }
        Arrays.sort(lista3);

        try {
            File archivo = new File("Lista.txt");
            try (FileWriter archi = new FileWriter(archivo)) {
                archi.write(Arrays.toString(lista) + "\n");
                archi.write(Arrays.toString(lista2) + "\n");
                archi.write(Arrays.toString(lista3));
            }

            FileReader read = new FileReader("Lista.txt");
            BufferedReader conten = new BufferedReader(read);
            String texto = "";
            while (conten.ready()) {
                texto = conten.readLine();
                System.out.println(texto);
            }
        } catch (IOException e) {
            System.out.println("Error al escribir en el archivo y LEERLO" + e);
        }
    }
}
