package clase2;

import java.util.Scanner;

/**
 *
 * @author Natalia
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        PalabrasRimadas rima = new PalabrasRimadas();
        EdadMatriz matriz = new EdadMatriz();
        Scanner sc = new Scanner(System.in);
        System.out.print("1.Rimar palabras\n"
                + "2.Pintar edades\n"
                + "3.Modificar edades\n"
                + "4.Salir\n"
                + "Digite opción deseada: ");
        int opcion = sc.nextInt();
        switch (opcion) {
            case 1:
                rima.hacerRima();
                break;
            case 2:
                matriz.pintarEdad();
                break;
            case 3:
                matriz.modificarEdad();
                break;
            case 4:
                System.out.println("Digite opción válida: ");
                opcion = sc.nextInt();
            default:
                System.exit(0);
        }
    }

}
