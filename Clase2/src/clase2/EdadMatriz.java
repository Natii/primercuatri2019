package clase2;

import java.util.Scanner;

/**
 *
 * @author Natalia
 */
public class EdadMatriz {
    
    String nombreP;
    String[] nombre = new String[2];
    int[] edadPerso = new int[2];
    int edad = 0;
    int par = 0, impar = 0;
    String numero;
    String rojo = "\033[34m";
    String azul = "\033[31m";

    public void pintarEdad() {
        Scanner entrada = new Scanner(System.in);
//        numero = (Integer.toString(edad));
        for (int i = 0; i < nombre.length; i++) {
            for (int x = 0; x < edadPerso.length; x++) {
                System.out.println("Ingrese nombre: ");
                nombreP = entrada.next();
                System.out.println("Ingrese edad: ");
                edad = entrada.nextInt();
                if (edad % 2 == 0) {
                    System.out.println("\033[34m");
                    System.out.println("La edad es un número par!!!");
                    System.out.println("La edad es: " + rojo + edad);
                } else {
                    System.out.println("\033[31m");
                    System.out.println("La edad es un número impar y la edad es: " + azul + edad);
                }
            }
//            System.out.println(nombre[i] + " " + edadPerso[x]);
            break;
        }
    }

    public void modificarEdad() {
        Scanner entrada = new Scanner(System.in);
        for (int x = 0; x < nombre.length; x++) {
            System.out.println("Ingrese nombre de la persona que desea modificarle la edad:");
            nombreP = entrada.next();
            for (int n = 0; n < edadPerso.length; n++) {
                System.out.println("Ingrese edad: ");
                edad = entrada.nextInt();
                if (edad % 2 == 0) {
                    System.out.println("\033[34m");
                    System.out.println("La edad es un número par");
                    System.out.println("La edad es: " + rojo + edad);
                } else {
                    System.out.println("\033[31m");
                    System.out.println("La edad es un número impar y la edad es: " + azul + edad);
                }
            }
        }
    }
    
}
