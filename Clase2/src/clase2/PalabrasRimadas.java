package clase2;

import java.util.Scanner;

/**
 *
 * @author Natalia
 */
public class PalabrasRimadas {
    
    Scanner ingresar = new Scanner(System.in);

    public void hacerRima() {
        while (true) {
            System.out.println("1.Digitar palabras\n 2.Atrás\n"
                    + "Seleccione opción: ");
            int opcion = ingresar.nextInt();
            switch (opcion) {
                case 1:
                    System.out.println("Digite una palabra");
                    String palabra1 = ingresar.next();
                    System.out.println("Digite otra palabra");
                    String palabra2 = ingresar.next();
                    if (palabra1.charAt(palabra1.length() - 1) == palabra2.charAt(palabra2.length() - 1) && palabra1.charAt(palabra1.length() - 2) == palabra2.charAt(palabra2.length() - 2) && palabra1.charAt(palabra1.length() - 3) == palabra2.charAt(palabra2.length() - 3)) {
                        System.out.println("Sí riman!!!");
                    } else if (palabra1.charAt(palabra1.length() - 1) == palabra2.charAt(palabra2.length() - 1) && palabra1.charAt(palabra1.length() - 2) == palabra2.charAt(palabra2.length() - 2)) {
                        System.out.println("Riman poco");
                    } else {
                        System.out.println("No riman!!!");
                    }
                    break;
                case 2:
                    System.out.println("Error! Digite opción válida!!");
                    hacerRima();
                    break;
                default:
                    System.exit(0);
                    break;
            }
        }
    }
    
}
