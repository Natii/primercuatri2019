package archivovehiculo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import archivovehiculo.LimpiarTxt;
import archivovehiculo.Vehiculo;
import archivovehiculo.RegistroVehiculo;

/**
 *
 * @author Natalia
 */
public class ProcesoArchivo {

    private ArrayList<Object> a = new ArrayList<Object>();
    public String rutaTxt = "Vehiculos.txt";
    RegistroVehiculo registro = new RegistroVehiculo(null, true);
    Deportivo dep = new Deportivo();
    Furgoneta fur = new Furgoneta();
    Vehiculo veh = new Vehiculo();
    ProcesoArchivo proAr = new ProcesoArchivo();
    LimpiarTxt lt = new LimpiarTxt();

//    public ProcesoArchivo(ArrayList<Object> a) {
//        this.a = a;
//    }
    public String cargarTxt() {
        File ruta = new File(rutaTxt);
        String linea = "";
        try {
            FileReader fr = new FileReader(ruta);
            BufferedReader br = new BufferedReader(fr);

            while ((linea = br.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(linea, ", ");
                if (this.registro.chkDeportivo.isSelected()) {
                    dep = new Deportivo();
                    dep.setMatricula(st.nextToken());
                    dep.setMarca(st.nextToken());
                    dep.setPrecio(st.nextToken());
                    dep.setColor(st.nextToken());
                    dep.setPotencia(st.nextToken());
                    dep.setVelMaxima(st.nextToken());
                    proAr.agregarRegistro(veh, dep, null);
                } else if (this.registro.chkFurgoneta.isSelected()) {
                    fur = new Furgoneta();
                    fur.setMatricula(st.nextToken());
                    fur.setMarca(st.nextToken());
                    fur.setPrecio(st.nextToken());
                    fur.setColor(st.nextToken());
                    fur.setCapacidadPersonas(st.nextToken());
                    fur.setDisponible(st.nextToken());
                    proAr.agregarRegistro(veh, null, fur);
                }
            }
            linea = br.readLine();
            br.close();
        } catch (Exception ex) {
            mostrarMensaje("Error al cargar archivo: " + ex.getMessage());
            System.out.println(ex.getMessage());
        }
        return linea;
    }

    public void grabarTxt() {
        FileWriter fw;
        PrintWriter pw;
        try {
            fw = new FileWriter(rutaTxt);
            pw = new PrintWriter(fw);

            for (int i = 0; i < proAr.cantidadRegistro(); i++) {
                veh = proAr.obtenerRegistro(i);
                dep = proAr.obtenerRegistroDep(i);
                fur = proAr.obtenerRegistroFur(i);
                pw.println(String.valueOf(veh.getMatricula() + ", " + veh.getMarca()
                        + ", " + veh.getPrecio() + ", " + veh.getColor() + ", " + dep.getMatricula()
                        + ", " + dep.getMarca() + ", " + dep.getPrecio() + ", " + dep.getColor()
                        + ", " + dep.getPotencia() + ", " + dep.getVelMaxima()
                        + ", " + fur.getMatricula() + ", " + fur.getMarca() + ", " + fur.getPrecio()
                        + ", " + fur.getColor() + ", " + fur.getCapacidadPersonas()
                        + ", " + fur.isDisponible()));
            }
            pw.close();
        } catch (Exception ex) {
            mostrarMensaje("Error al grabar archivo: " + ex.getMessage());
            System.out.println(ex.getMessage());
        }
    }

    public void ingresarRegistro(File ruta, String registro) {
        try {
            if (!ruta.exists()) {
                ruta.createNewFile();
            }
            if (this.registro.chkDeportivo.isSelected() == true) {
                this.registro.jpDeportivo.setEnabled(true);
                this.registro.jtfPotencia.setEnabled(true);
                this.registro.jtfVelocidadMax.setEnabled(true);
                if (leerMatricula() == null) {
                    mostrarMensaje("Ingresa matrícula");
                } else if (leerMarca() == null) {
                    mostrarMensaje("Ingresa marca");
                } else if (leerPrecio() == null) {
                    mostrarMensaje("Ingresar Precio");
                } else if (leerColor() == null) {
                    mostrarMensaje("Ingresa color");
                } else if (leerPotencia() == null) {
                    mostrarMensaje("Ingresa potencia");
                } else if (leerVelocidadMax() == null) {
                    mostrarMensaje("Ingresa velocidad máxima");
                }
            } else if (this.registro.chkFurgoneta.isSelected() == true) {
                this.registro.jpFurgoneta.setEnabled(true);
                this.registro.jtfCapacidad.setEnabled(true);
                this.registro.cmbDisponibilidad.setEnabled(true);
                if (leerMatricula() == null) {
                    mostrarMensaje("Ingresa matrícula");
                } else if (leerMarca() == null) {
                    mostrarMensaje("Ingresa marca");
                } else if (leerPrecio() == null) {
                    mostrarMensaje("Ingresar Precio");
                } else if (leerColor() == null) {
                    mostrarMensaje("Ingresa color");
                } else if (leerCapacidadPerso() == null) {
                    mostrarMensaje("Ingresa capacidad para personas");
                } else if (leerDisponibilidad() == null) {
                    mostrarMensaje("Ingresa disponibilidad");
                }
            } else {
                veh = new Vehiculo(leerMatricula(), leerMarca(), leerPrecio(), leerColor());
                dep = new Deportivo(leerMatricula(), leerMarca(), leerPrecio(), leerColor(),
                        leerPotencia(), leerVelocidadMax());
                fur = new Furgoneta(leerMatricula(), leerMarca(), leerPrecio(), leerColor(),
                        leerCapacidadPerso(), leerDisponibilidad());
                if (proAr.buscarCodigo(veh.getMatricula()) != -1) {
                    mostrarMensaje("Esta matrícula ya existe");
                } else {
                    proAr.agregarRegistro(veh, dep, fur);
                }
                grabarTxt();
                listarRegistro();
                lt.limpiarTexto(this.registro.jpVehiculo);
            }
        } catch (Exception ex) {
            mostrarMensaje(ex.getMessage());
        }
    }

    public void modificarRegistro(File ruta) {
        try {
            if (leerMatricula() == null) {
                mostrarMensaje("Ingresa matrícula");
            } else if (leerMarca() == null) {
                mostrarMensaje("Ingresa marca");
            } else if (leerPrecio() == null) {
                mostrarMensaje("Ingresa Precio");
            } else if (leerColor() == null) {
                mostrarMensaje("Ingresa color");
            } else if (this.registro.chkDeportivo.isSelected()) {
                this.registro.jpFurgoneta.disable();
                this.registro.jtfCapacidad.disable();
                this.registro.cmbDisponibilidad.disable();
                if (leerPotencia() == null) {
                    mostrarMensaje("Ingresa potencia");
                } else if (leerVelocidadMax() == null) {
                    mostrarMensaje("Ingresa velocidad máxima");
                }
            } else if (this.registro.chkFurgoneta.isSelected()) {
                this.registro.jpDeportivo.disable();
                this.registro.jtfPotencia.disable();
                this.registro.jtfVelocidadMax.disable();
                if (leerCapacidadPerso() == null) {
                    mostrarMensaje("Ingresa capacidad para personas");
                } else if (leerDisponibilidad() == null) {
                    mostrarMensaje("Ingresa disponibilidad");
                }
            } else {
                int matricula = proAr.buscarCodigo(leerMatricula());
                veh = new Vehiculo(leerMatricula(), leerMarca(), leerPrecio(), leerColor());
                dep = new Deportivo(leerMatricula(), leerMarca(), leerPrecio(), leerColor(),
                        leerPotencia(), leerVelocidadMax());
                fur = new Furgoneta(leerMatricula(), leerMarca(), leerPrecio(), leerColor(),
                        leerCapacidadPerso(), leerDisponibilidad());
                if (matricula == -1) {
                    proAr.agregarRegistro(veh, dep, fur);
                } else {
                    proAr.modificarRegistro(matricula, veh, dep, fur);
                }

                grabarTxt();
                listarRegistro();
                lt.limpiarTexto(this.registro.jpVehiculo);
            }
        } catch (Exception ex) {
            mostrarMensaje(ex.getMessage());
        }
    }

    public void listarRegistro() {
        DefaultTableModel dtm = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        dtm.addColumn("Matrícula");
        dtm.addColumn("Marca");
        dtm.addColumn("Precio");
        dtm.addColumn("Color");
        dtm.addColumn("Potencia");
        dtm.addColumn("Velocidad Máx.");
        dtm.addColumn("Capacidad Personas");
        dtm.addColumn("Disponibilidad");

        Object fila[] = new Object[dtm.getColumnCount()];
        for (int i = 0; i < proAr.cantidadRegistro(); i++) {
            veh = proAr.obtenerRegistro(i);
            dep = proAr.obtenerRegistroDep(i);
            fur = proAr.obtenerRegistroFur(i);
            fila[0] = veh.getMatricula();
            fila[1] = veh.getMarca();
            fila[2] = veh.getPrecio();
            fila[3] = veh.getColor();
            fila[4] = dep.getPotencia();
            fila[5] = dep.getVelMaxima();
            fila[6] = fur.getCapacidadPersonas();
            fila[7] = fur.isDisponible();
            dtm.addRow(fila);
        }
        this.registro.jtTablaVehiculos.setModel(dtm);
        this.registro.jtTablaVehiculos.setRowHeight(75);
    }

    public String leerMatricula() {
        try {
            String matricula = (this.registro.jtfPrecio.getText().trim());
            return matricula;
        } catch (Exception ex) {
            return null;
        }
    }

    public String leerMarca() {
        try {
            String marca = (this.registro.cmbMarca.getSelectedItem().toString());
            return marca;
        } catch (Exception ex) {
            return null;
        }
    }

    public String leerPrecio() {
        try {
            String precio = (this.registro.jtfPrecio.getText().trim());
            return precio;
        } catch (Exception ex) {
            return null;
        }
    }

    public String leerColor() {
        try {
            String color = (this.registro.cmbColor.getSelectedItem().toString());
            return color;
        } catch (Exception ex) {
            return null;
        }
    }

    public String leerPotencia() {
        try {
            String potencia = (this.registro.jtfPotencia.getText().trim());
            return potencia;
        } catch (Exception ex) {
            return null;
        }
    }

    public String leerVelocidadMax() {
        try {
            String veloMax = (this.registro.jtfVelocidadMax.getText().trim());
            return veloMax;
        } catch (Exception ex) {
            return null;
        }
    }

    public String leerCapacidadPerso() {
        try {
            String capacidad = (this.registro.jtfCapacidad.getText().trim());
            return capacidad;
        } catch (Exception ex) {
            return null;
        }
    }

    public String leerDisponibilidad() {
        try {
            String dispo = (this.registro.cmbDisponibilidad.getSelectedItem().toString());
            return dispo;
        } catch (Exception ex) {
            return null;
        }
    }

    public void mostrarMensaje(String texto) {
        JOptionPane.showMessageDialog(null, texto);
    }

    public void agregarRegistro(Vehiculo ve, Deportivo depor, Furgoneta furgo) {
        this.a.add(ve);
        this.a.add(depor);
        this.a.add(furgo);
    }

    public void modificarRegistro(int i, Vehiculo ve, Deportivo depor, Furgoneta furgo) {
        this.a.set(i, ve);
        this.a.set(i, depor);
        this.a.set(i, furgo);
    }

    public void eliminarRegistro(int i) {
        this.a.remove(i);
    }

    public Vehiculo obtenerRegistro(int i) {
        return (Vehiculo) a.get(i);
    }

    public Deportivo obtenerRegistroDep(int i) {
        return (Deportivo) a.get(i);
    }

    public Furgoneta obtenerRegistroFur(int i) {
        return (Furgoneta) a.get(i);
    }

    public int cantidadRegistro() {
        return this.a.size();
    }

    public int buscarCodigo(String matricula) {
        for (int i = 0; i < cantidadRegistro(); i++) {
            if (matricula.equals(obtenerRegistro(i).getMatricula())) {
                return i;
            }
        }
        return -1;
    }

}
