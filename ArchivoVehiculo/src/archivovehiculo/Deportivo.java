package archivovehiculo;

/**
 *
 * @author Natalia
 */
public class Deportivo extends Vehiculo {

    String velMaxima, potencia;

    public Deportivo() {
    }

    public Deportivo(String marca, String color, String precio, String matricula, String velMaxima, String potencia) {
        super(marca, color, precio, matricula);
        this.potencia = potencia;
        this.velMaxima = velMaxima;
    }

    public String getVelMaxima() {
        return velMaxima;
    }

    public String getPotencia() {
        return potencia;
    }

    @Override
    public String getMarca() {
        return marca;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public String getMatricula() {
        return matricula;
    }

    @Override
    public String getPrecio() {
        return precio;
    }

    public void setVelMaxima(String velMaxima) {
        this.velMaxima = velMaxima;
    }

    public void setPotencia(String potencia) {
        this.potencia = potencia;
    }

    @Override
    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Override
    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    @Override
    public void setPrecio(String precio) {
        this.precio = precio;
    }

}
