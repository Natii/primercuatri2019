package archivovehiculo;

/**
 *
 * @author Natalia
 */
public class Furgoneta extends Vehiculo {

    String capacidadPersonas;
    String disponible;

    public Furgoneta() {
    }

    public Furgoneta(String capacidadPersonas, String disponible, String marca, String color, String precio, String matricula) {
        super(marca, color, precio, matricula);
        this.capacidadPersonas = capacidadPersonas;
        this.disponible = disponible;
    }

    public String getCapacidadPersonas() {
        return capacidadPersonas;
    }

    public String isDisponible() {
        return disponible;
    }

    @Override
    public String getMarca() {
        return marca;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public String getMatricula() {
        return matricula;
    }

    @Override
    public String getPrecio() {
        return precio;
    }

    public void setCapacidadPersonas(String capacidadPersonas) {
        this.capacidadPersonas = capacidadPersonas;
    }

    public void setDisponible(String disponible) {
        this.disponible = disponible;
    }

    @Override
    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Override
    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    @Override
    public void setPrecio(String precio) {
        this.precio = precio;
    }

}
