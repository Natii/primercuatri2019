package archivovehiculo;

/**
 *
 * @author Natalia
 */
public class Vehiculo {

    String marca, color, matricula;
    String precio;
    Deportivo dep = new Deportivo();
    Furgoneta fur = new Furgoneta();

    public Vehiculo() {
    }

    public Vehiculo(String matricula, String marca, String color, String precio) {
        this.matricula = matricula;
        this.marca = marca;
        this.color = color;
        this.precio = precio;
    }

    public String enviarDatos() {
        String linea = "";
        ProcesoArchivo archivo = new ProcesoArchivo();
        String resul = archivo.cargarTxt();
        String[] datos = resul.split(", ");
        matricula = datos[0];
        marca = datos[1];
        color = datos[2];
        precio = datos[3];
        dep.potencia = datos[4];
        dep.velMaxima = datos[5];
        fur.disponible = datos[6];
        fur.capacidadPersonas = datos[7];
        return linea;
    }

    public String getMarca() {
        return marca;
    }

    public String getColor() {
        return color;
    }

    public String getMatricula() {
        return matricula;
    }

    public String getPrecio() {
        return precio;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }
}
