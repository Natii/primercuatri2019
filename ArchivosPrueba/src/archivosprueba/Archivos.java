/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package archivosprueba;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Natalia
 */
public class Archivos {
    File ruta = new File("C:\\Users\\Natalia\\Documents\\Universidad\\2019\\ICuatri\\Progra2\\ArchivosPrueba\\Naty.txt");
    
     public void IngresarArchivoSuperUsuario(String nombre, String contra){
        FileWriter fw;
        BufferedWriter bw;
        
        try{
        
        if(ruta.exists()){
            fw = new FileWriter(ruta,true);
            bw = new BufferedWriter(fw);
            bw.write("Nombre de usuario: " + nombre + "\n"
                     + "Contraseña: " + contra);
            
        }else{
            fw = new FileWriter(ruta,true);
            bw = new BufferedWriter(fw);
            bw.newLine();
            bw.write("Nombre de usuario: " + nombre + "\n"
                     + "Contraseña: " + contra);
        }
        bw.close();
        fw.close();
        
        }catch(IOException e){
            JOptionPane.showMessageDialog(null, "Se registro con exito: " );
        }
   }
    
    public void EscribirArchivo(String nombreArchivo, String tipoTexto){
    
    try{
    
        FileWriter fw = new FileWriter(ruta, true);
        BufferedWriter bw = new BufferedWriter(fw);
        
        ruta = new File(nombreArchivo);
        bw.write(tipoTexto);
        bw.close();
        fw.close();
    
    }catch(IOException e){
        JOptionPane.showMessageDialog(null, "Error" + e);
    }
  }
    
    public void LeerArchivo(String nombreArchivo) throws FileNotFoundException, IOException{
        FileReader r = new FileReader(nombreArchivo);
        BufferedReader buffer = new BufferedReader(r);
        
        String temp = " ";
            
        while(temp!=null)
        {
        
            temp=buffer.readLine();
            
            if(temp == null)
                break;
            
            System.out.println(temp);
                
        }
        
    }
}
