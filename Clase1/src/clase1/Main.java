package clase1;

import java.util.Scanner;

/**
 *
 * @author Natalia
 */
public class Main {
    
    Scanner sc = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        new Main().calcularNumeroSuerte();
        new Main().calcularNumeroPerfecto();
        new Main().adivinarNumero();
        
    }
        public void calcularNumeroSuerte() {
        int dia, mes, año, numSuerte, sumaFecha, numero1, numero2, numero3, numero4;
        System.out.println("Introduce fecha de nacimiento");
        System.out.print("Día: ");
        dia = sc.nextInt();
        System.out.print("Mes: ");
        mes = sc.nextInt();
        System.out.print("Año: ");
        año = sc.nextInt();
        sumaFecha = dia + mes + año;
        numero1 = sumaFecha / 1000;
        numero2 = sumaFecha / 100 % 10;
        numero3 = sumaFecha / 10 % 10;
        numero4 = sumaFecha % 10;
        numSuerte = numero1 + numero2 + numero3 + numero4;
        System.out.println("Tu número de la suerte es: " + numSuerte);
    }

    public void calcularNumeroPerfecto() {
        int numero;
        int suma = 0;
        System.out.println("Introduce un numero");
        System.out.print("Numero: ");
        numero = sc.nextInt();
        for (int i = 1; i > numero - 1; i++) {
            if (numero % i == 0) {
                suma += i;
            }
        }
        if (suma == numero) {
            System.out.println(numero + " es un numero perfecto");
        } else {
            System.out.println(numero + " es un numero imperfecto");
        }
    }

    public void adivinarNumero() {
        int numero = 0;
        int intento = 5;
        int numeroAleatorio = (int) (Math.random() * 99);
        do {
            System.out.println("Ingresa un número entre 0 y 99 ");
            numero = sc.nextInt();
            if (numero < numeroAleatorio && intento <= 5) {
                intento--;
                System.out.println(" El número aleatorio es mayor."
                        + " Te quedan " + intento + " intentos");
            } else if (numero > numeroAleatorio && intento <= 5) {
                intento--;
                System.out.println(" El número aleatorio es menor."
                        + " Te quedan " + intento + " intentos");
            } else if (numero == numeroAleatorio && intento <= 5) {
                intento--;
                System.out.println(" Has adivinado el número!!!");
            } else if (numeroAleatorio != numero && intento > 1) {
                System.out.println(" Perdiste los intentos ");
                break;
            }
        } while (numero != -1);
        System.out.println(" Ha salido del juego!!! ");
    }
    }
