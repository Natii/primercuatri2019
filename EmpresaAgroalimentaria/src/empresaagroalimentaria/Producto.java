package empresaagroalimentaria;

/**
 *
 * @author Natalia
 */
public class Producto {

    public String fechaCaducidad;
    public String numeroLote;

    public Producto(String fechaCaducidad, String numeroLote) {
        this.fechaCaducidad = fechaCaducidad;
        this.numeroLote = numeroLote;
    }

    public String getAtributos() {
        return "Fecha de caducidad: " + fechaCaducidad
             + "\nNúmero de lote: " + numeroLote;
    }

    public void tipoProducto() {
        System.out.println("¿Qué tipo de producto es?");
    }
}
