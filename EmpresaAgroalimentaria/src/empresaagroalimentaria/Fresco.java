package empresaagroalimentaria;

/**
 *
 * @author Natalia
 */
public class Fresco extends Producto {

    String fechaEnvasado, paisOrigen;

    public Fresco(String fechaCaducidad, String numeroLote, String fechaEnvasado, String paisOrigen) {
        super(fechaCaducidad, numeroLote);
        this.fechaEnvasado = fechaEnvasado;
        this.paisOrigen = paisOrigen;
    }

    @Override
    public String getAtributos() {
        return "Fecha de caducidad: " + fechaCaducidad
                + "\nNúmero de lote: " + numeroLote
                + "\nFecha de envasado:" + fechaEnvasado
                + "\nPaís de Origen: " + paisOrigen;
    }

    @Override
    public void tipoProducto() {
        System.out.println("El producto es fresco.");
    }
}
