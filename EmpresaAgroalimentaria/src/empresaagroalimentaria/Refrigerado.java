package empresaagroalimentaria;

/**
 *
 * @author Natalia
 */
public class Refrigerado extends Producto {

    String codigoOrgSup, fechaEnvasado, temperatura, paisOrigen;

    public Refrigerado(String fechaCaducidad, String numeroLote, String codigoOrgSup, String fechaEnvasado, String temperatura, String paisOrigen) {
        super(fechaCaducidad, numeroLote);
        this.codigoOrgSup = codigoOrgSup;
        this.fechaEnvasado = fechaEnvasado;
        this.temperatura = temperatura;
        this.paisOrigen = paisOrigen;
    }

    @Override
    public String getAtributos() {
        return "Fecha de caducidad: " + fechaCaducidad
                + "\nNúmero de lote: " + numeroLote
                + "\nCódigo de la Organización de Supervisión Alimentaria: " + codigoOrgSup
                + "\nFecha de envasado: " + fechaEnvasado
                + "\nTemperatura de mantenimiento recomendada: " + temperatura
                + "\nPaís de Origen: " + paisOrigen;
    }

    @Override
    public void tipoProducto() {
        System.out.println("El producto es refrigerado.");
    }

}
