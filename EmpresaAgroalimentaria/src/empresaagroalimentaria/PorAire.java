package empresaagroalimentaria;

/**
 *
 * @author Natalia
 */
public class PorAire extends Congelado {

    String porcentNitro, porcentOxige, porcentCo2, vaporAgua;

    public PorAire(String fechaCaducidad, String numeroLote, String fechaEnvasado, String paisOrigen, String temperatura, String porcentNitro, String porcentOxige, String porcentCo2, String vaporAgua) {
        super(fechaCaducidad, numeroLote, fechaEnvasado, paisOrigen, temperatura);
        this.porcentNitro = porcentNitro;
        this.porcentOxige = porcentOxige;
        this.porcentCo2 = porcentCo2;
        this.vaporAgua = vaporAgua;
    }

    @Override
    public String getAtributos() {
        return "Fecha de caducidad: " + fechaCaducidad
                + "\nNúmero de lote: " + numeroLote
                + "\nFecha de envasado: " + fechaEnvasado
                + "\nPaís de Origen: " + paisOrigen
                + "\nTemperatura de mantenimiento recomendada: " + temperatura
                + "\nPorcentaje de Nitrógeno: " + porcentNitro
                + "\nPorcentaje de Oxígeno: " + porcentOxige
                + "\nPorcentaje de Dióxido de Carbono: " + porcentCo2
                + "\nPorcentaje de Vapor de Agua: " + vaporAgua;
    }

    @Override
    public void tipoProducto() {
        System.out.println("El producto es congelado por aire.");
    }

}
