package empresaagroalimentaria;

/**
 *
 * @author Natalia
 */
public class PorNitrogeno extends Congelado {

    String metodoCongelacion, tiempoExpo;

    public PorNitrogeno(String fechaCaducidad, String numeroLote, String fechaEnvasado, String paisOrigen, String temperatura, String metodoCongelacion, String tiempoExpo) {
        super(fechaCaducidad, numeroLote, fechaEnvasado, paisOrigen, temperatura);
        this.metodoCongelacion = metodoCongelacion;
        this.tiempoExpo = tiempoExpo;
    }

    @Override
    public String getAtributos() {
        return "Fecha de caducidad: " + fechaCaducidad
                + "\nNúmero de lote: " + numeroLote
                + "\nFecha de envasado: " + fechaEnvasado
                + "\nPaís de Origen: " + paisOrigen
                + "\nTemperatura de mantenimiento recomendada: " + temperatura
                + "\nEl método de congelación es: " + metodoCongelacion
                + "\nEl tiempo de exposición al Nitrógeno expresado en segundos: " + tiempoExpo;
    }

    @Override
    public void tipoProducto() {
        System.out.println("El producto es congelado por nitrógeno.");
    }

}
