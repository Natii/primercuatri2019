package empresaagroalimentaria;

/**
 *
 * @author Natalia
 */
public class PorAgua extends Congelado {
    
    String salinidadAgua;

    public PorAgua(String fechaCaducidad, String numeroLote, String fechaEnvasado, String paisOrigen, String temperatura, String salinidadAgua) {
        super(fechaCaducidad, numeroLote, fechaEnvasado, paisOrigen, temperatura);
        this.salinidadAgua = salinidadAgua;
    }
    
        @Override
    public String getAtributos() {
        return "Fecha de caducidad: " + fechaCaducidad
                + "\nNúmero de lote: " + numeroLote
                + "\nFecha de envasado: " + fechaEnvasado
                + "\nPaís de Origen: " + paisOrigen
                + "\nTemperatura de mantenimiento recomendada: " + temperatura
                + "\nLa salinidad del agua es: " + salinidadAgua;
    }

    @Override
    public void tipoProducto() {
        System.out.println("El producto es congelado por agua.");
    }
    
}
