package empresaagroalimentaria;

import java.util.Scanner;

/**
 *
 * @author Natalia
 */
public class Congelado extends Producto {

    String fechaEnvasado, paisOrigen, temperatura;
    Scanner sc = new Scanner(System.in);

    public Congelado(String fechaCaducidad, String numeroLote, String fechaEnvasado, String paisOrigen, String temperatura) {
        super(fechaCaducidad, numeroLote);
        this.fechaEnvasado = fechaEnvasado;
        this.paisOrigen = paisOrigen;
        this.temperatura = temperatura;
    }

    @Override
    public String getAtributos() {
        return "Fecha de caducidad: " + fechaCaducidad
                + "\nNúmero de lote: " + numeroLote
                + "\nFecha de envasado: " + fechaEnvasado
                + "\nPaís de Origen: " + paisOrigen
                + "\nTemperatura de mantenimiento recomendada: " + temperatura;
    }

    @Override
    public void tipoProducto() {
        System.out.println("El producto es congelado.");
    }
    
    public void informacion() {
        System.out.println("Digite fecha de caducidad: " );
        String respuesta = sc.next();
        
    }

}
