package ejemploherencia;

import java.util.Scanner;

/**
 *
 * @author Natalia
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        SeleccionFutbol sele = new SeleccionFutbol(0, 0, "", "");
        Entrenador entrenador = new Entrenador(0, 0, 0, "", "");
        Futbolista futbolista = new Futbolista(0, "", 0, 0, "", "");
        Masajista masajista = new Masajista("", 0, 0, 0, "", "");
        Scanner sc = new Scanner(System.in);

        sele.tipoPersona();
        String respuesta = sc.next();
        switch (respuesta) {
            case "E":
                entrenador.tipoPersona();
                entrenador.dirigirEntrenamiento();
                entrenador.dirigirPartido();
                break;
            case "F":
                futbolista.tipoPersona();
                futbolista.concentrarse();
                futbolista.entrenar();
                futbolista.jugarPartido();
                break;
            case "M":
                masajista.tipoPersona();
                masajista.concentrarse();
                masajista.darMasaje();
                break;
            default:
                break;
        }
    }
}
