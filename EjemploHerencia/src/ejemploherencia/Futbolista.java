package ejemploherencia;

import java.util.Scanner;

/**
 *
 * @author Natalia
 */
public class Futbolista extends SeleccionFutbol {

    int dorsal;
    String demarcación;
    Scanner sc = new Scanner(System.in);

    public Futbolista(int dorsal, String demarcación, int id, int edad, String nombre, String apellidos) {
        super(id, edad, nombre, apellidos);
        this.dorsal = dorsal;
        this.demarcación = demarcación;
    }

    @Override
    public String toString() {
        return "Id: " + id
                + "\nNombre: " + nombre
                + "\nApellidos:" + apellidos
                + "\nEdad: " + edad
                + "\nDorsal: " + dorsal
                + "\nDemarcación: " + demarcación;
    }

    public void jugarPartido() {
        System.out.println("Está listo para jugar? S/N");
        String respuesta = this.sc.next();
        if (respuesta.equals(true)) {
            System.out.println("Bien");
        } else {
            System.out.println("No juegue");
        }
    }

    public void entrenar() {
        System.out.println("Ya entrenó? S/N");
        String respu = this.sc.next();
        if (respu.equals(true)) {
            System.out.println("Perfecto!!!");
        } else {
            System.out.println("Debería entrenar");
        }

    }

    @Override
    public void tipoPersona() {
        System.out.println("Es futbolista");
    }

}
