package ejemploherencia;

import java.util.Scanner;

/**
 *
 * @author Natalia
 */
public class Masajista extends SeleccionFutbol {

    String titulacion;
    int aniosExperiencia;
    Scanner sc = new Scanner(System.in);

    public Masajista(String titulacion, int aniosExperiencia, int id, int edad, String nombre, String apellidos) {
        super(id, edad, nombre, apellidos);
        this.titulacion = titulacion;
        this.aniosExperiencia = aniosExperiencia;
    }

    @Override
    public String toString() {
        return "Id: " + id
                + "\nNombre: " + nombre
                + "\nApellidos: " + apellidos
                + "\nEdad: " + edad
                + "\nTitulación: " + titulacion
                + "\nAños de experiencia: " + aniosExperiencia;
    }

    public void darMasaje() {
        System.out.println("Ya dio el masaje? S/N");
        String respuesta = this.sc.next();
        if (respuesta.equals(true)) {
            System.out.println("Bien!!! Hizo su trabajo");
        } else {
            System.out.println("Realice su trabajo");
        }
    }

    @Override
    public void tipoPersona() {
        System.out.println("Es masajista");
    }
}
