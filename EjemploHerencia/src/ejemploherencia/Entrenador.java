package ejemploherencia;

import java.util.Scanner;

/**
 *
 * @author Natalia
 */
public class Entrenador extends SeleccionFutbol {

    int idFederacion;
    Scanner sc = new Scanner(System.in);

    public Entrenador(int idFederacion, int id, int edad, String nombre, String apellidos) {
        super(id, edad, nombre, apellidos);
        this.idFederacion = idFederacion;
    }

    @Override
    public String toString() {
        return "Id: " + id
                + "\nNombre: " + nombre
                + "\nApellidos: " + apellidos
                + "\nEdad: " + nombre
                + "\nId Federación: " + idFederacion;
    }

    public void dirigirPartido() {
        System.out.println("Desea dirigir el partido? S/N");
        String respuesta = this.sc.next();
        if (respuesta.equals(true)) {
            System.out.println("Suerte!!!");
        } else {
            System.out.println("Adiós");
        }
    }

    public void dirigirEntrenamiento() {
        System.out.println("Ya dirigió el entrenamiento? S/N");
        String respuesta = this.sc.next();
        if (respuesta.equals(true)) {
            System.out.println("Felicidades!!! Descanse");
        } else {
            System.out.println("Vaya diríjalo");
        }
    }

    @Override
    public void tipoPersona() {
        System.out.println("Es entrenador");
    }

}
