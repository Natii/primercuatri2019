package ejemploherencia;

import java.util.Scanner;

/**
 *
 * @author Natalia
 */
public class SeleccionFutbol {

    public int id, edad;
    public String nombre, apellidos, concentracion, viaje;
    Scanner sc = new Scanner(System.in);

    public SeleccionFutbol(int id, int edad, String nombre, String apellidos) {
        this.id = id;
        this.edad = edad;
        this.nombre = nombre;
        this.apellidos = apellidos;
    }

    @Override
    public String toString() {
        return "Id: " + id
                + "\nEdad: " + edad
                + "\nNombre: " + nombre
                + "\nApellidos: " + apellidos;
    }

    public void concentrarse() {
        if (concentracion.equals(true)) {
            System.out.println("Felicidades!!! Continúe así");
        } else {
            System.out.println("Debe concentrarse más");
        }
    }

    public void viajar() {
        System.out.println("Desea viajar? S/N");
        viaje = this.sc.next();
        if (viaje.equals(true)) {
            System.out.println("Digite país a visitar? ");
            String pais = this.sc.next();
        } else {
            System.out.println("Debería considerar viajar");
        }

    }

    public void tipoPersona() {
        String persona = "";
        System.out.println("Qué tipo de persona es?");
        if (persona.equals('E') || persona.equals('e')) {
            //clase Entrenador
        } else if (persona.equals('F') || persona.equals('f')) {
            //clase Futbolista
        } else if (persona.equals('M') || persona.equals('m')) {
            //clase Masajista
        } else {
            System.out.println("Digite una persona válida");
        }
    }

}
